package com.example.staggeredrecycleview.model.remote

interface CategoryApi {
    suspend fun getCategory(): List<String>
}