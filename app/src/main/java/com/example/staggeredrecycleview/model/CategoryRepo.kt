package com.example.staggeredrecycleview.model

import com.example.staggeredrecycleview.model.remote.CategoryApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object CategoryRepo {

    private val categoryList = object : CategoryApi {
        override suspend fun getCategory(): List<String> {
            return listOf("Strings", "Floats", "Integers")
        }
    }

    suspend fun getCategory(): List<String> = withContext(Dispatchers.IO) {
        return@withContext categoryList.getCategory()
    }
}