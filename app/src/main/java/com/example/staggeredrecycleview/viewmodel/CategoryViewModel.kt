package com.example.staggeredrecycleview.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.staggeredrecycleview.model.CategoryRepo
import kotlinx.coroutines.launch

class CategoryViewModel : ViewModel() {

    // Instantiate repo
    private val repo = CategoryRepo

    // Initialize mutable live data
    private val _state = MutableLiveData<List<String>>()
    val state : LiveData<List<String>> get() = _state

    // function to launch new coroutine
    fun getCategory() {
        viewModelScope.launch {
            val category = repo.getCategory()
            _state.value = category
        }
    }

}